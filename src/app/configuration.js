const configuration = {
//   ApiUrl: "http://localhost:4000",
  ApiUrl: "https://backend-a3rc.onrender.com/",
  
  setApiRequestToken: (token) => {
    localStorage.setItem("requestToken", JSON.stringify(token));
  },
  removeApiRequestToken: () => {
    localStorage.removeItem("requestToken");
  },
  getAPIRequestToken: () => {
    return JSON.parse(localStorage.getItem("requestToken"));
  },
};

export const serverURL = "http://localhost:3000";

export default configuration;

const userNameTestPaypay = "sb-m57ir8943048@personal.example.com";

//sb-hac3z24018438@personal.example.com
//Cuong@28



