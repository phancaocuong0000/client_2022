import { Col, Container } from "@themesberg/react-bootstrap";
import { Tabs } from "antd";
import { getAllTopicFullList } from "app/core/apis/topic";
import React, { useEffect, useState } from "react";
import "./PracticePage.css";
import TabPane2 from "./TabPane2";
const { TabPane } = Tabs;

const PracticePage = () => {
  const [allTopic, setAllTopic] = useState([]);

  useEffect(() => {
    (async () => {
      try {
        const response1 = await getAllTopicFullList();
        setAllTopic(response1?.data?.topic);
      } catch (error) {}
    })();
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  return (
    <Container className="d-flex container-card justify-content-center">
      <Col className="layout-container-top exam mx-3">
        <div>
          <h1 className="text-center">Kiến thức cơ bản để thực hành</h1>
        </div>
        <Tabs
          className="tab-practice"
          defaultActiveKey="1"
          onChange={onChange}
          destroyInactiveTabPane
        >
          <TabPane tab="Câu hỏi theo chủ đề" key="2">
            <TabPane2 allTopic={allTopic} />
          </TabPane>
        </Tabs>
      </Col>
    </Container>
  );
};

export default PracticePage;
