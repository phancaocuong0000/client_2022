import { Col, Container } from "@themesberg/react-bootstrap";
import { Breadcrumb, PageHeader } from "antd";
import bannerBg from "app/assets/img/to-chuc-thi-scaled.png";
import { getLessonByID } from "app/core/apis/lessons";
import React, { useEffect, useState } from "react";
import { Accordion } from "react-bootstrap";
import { useLocation, useParams, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { updateLoading } from "app/store/loadingReducer";

const TutorialPage = () => {
  const [data, setData] = useState("");
  const { slugSection } = useParams();
  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      try {
        dispatch(updateLoading(true));
        const response = await getLessonByID(slugSection);
        const temp = response?.data?.topicLesson[0];
        if (response?.status === 200) {
          if (temp?.status === undefined || temp?.status === "private") {
            window.location = "/404";
          } else {
            setData(temp);
          }
        }
      } catch (error) {
        alert(error);
      } finally {
        dispatch(updateLoading(false));
      }
    })();
  }, []);

  return (
    <>
      <div
        className={"pageBanner_def"}
        style={{ backgroundImage: `url(${bannerBg})` }}
      >
        <div className="container_common">
          <div className="content_common">
            <div className="ifm">
              <>
                <Col span={18}>
                  <PageHeader
                    className="site-page-header"
                    title={data?.title}
                    breadcrumbRender={() => (
                      <Breadcrumb>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>
                          <a href="/studyRoad">Learning Path</a>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                          <span
                            onClick={() => history.goBack()}
                            style={{ cursor: "pointer" }}
                          >
                            Topic
                          </span>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>Lesson</Breadcrumb.Item>
                      </Breadcrumb>
                    )}
                  />
                </Col>
              </>
            </div>
          </div>
        </div>
      </div>
      <Container className="d-flex container-card">
        <Col className="layout-container-top tutorial">
          <div>
            <h1>{data?.title}</h1>
            {data?.body !== undefined && (
              <div
                className="body-content"
                dangerouslySetInnerHTML={{
                  __html: decodeURIComponent(escape(window.atob(data?.body))),
                }}
              ></div>
            )}
          </div>

          <div className="border"></div>
        </Col>
        <Col className="layout-container-body tutorial">
          <PostIndex data={data} location={location} />
        </Col>
      </Container>
    </>
  );
};

const PostIndex = ({ data, location }) => {
  return (
    <div className="sticky-sidebar">
      <div className="post-index hidden-sm-down">
        {/* <div className="section-title-line">
          <h5 className="text-uppercase">Tables of content</h5>
          <hr className="filler-line"></hr>
        </div> */}
        <ul className="content-outline list-unstyled">
          {data?.currentSection?.postIndex?.map((item, index) => (
            <li className="content-outline__item content-outline__item--level-3">
              <a
                href={`#${item.id}`}
                className={
                  location.hash !== `#${item.id}`
                    ? index === 0 && location.hash === ""
                      ? `link active`
                      : `link`
                    : `link active`
                }
              >
                {item.title}
              </a>
            </li>
          ))}
        </ul>
      </div>
      <div className="post-index hidden-sm-down">
        <div className="section-title-line">
          <h5 className="text-uppercase"></h5>
          <hr className="filler-line"></hr>
        </div>
        <Accordion className="accordion-content-topic">
          {data?.sections?.map((item, index) => (
            <Accordion.Item key={item.id} eventKey={item.id}>
              <Accordion.Header>{`${item.title}`}</Accordion.Header>
              <Accordion.Body>
                {item?.lessons?.map((item2, index2) => (
                  <a
                    key={item2.id}
                    href={`/section${item2.slug}`}
                  >{`${item2.title}`}</a>
                ))}
              </Accordion.Body>
            </Accordion.Item>
          ))}
        </Accordion>
      </div>
    </div>
  );
};
export default TutorialPage;
