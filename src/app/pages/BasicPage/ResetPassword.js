
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faEnvelope, faUnlockAlt } from "@fortawesome/free-solid-svg-icons";
import { Col, Row, Form, Card, Button, Container, InputGroup } from '@themesberg/react-bootstrap';
import { Link } from 'react-router-dom';
import * as yup from "yup";
import { Routes } from "app/routes";
import {resetPass} from "app/core/apis/user"
import { useHistory } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useParams } from "react-router-dom";
import "./Register.css";


const ResetPassword = () => {
  const history = useHistory();
 const { id } = useParams();

  const schema = yup
  .object()
  .shape({
    password: yup
    .string()
     .required("Password is required")
    .min(8, "Minimum must be greater 8 characters")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password has at least 1 uppercase, 1 digit, 1 special character and minimum 8 in length"
    ),

    confirmPassword: yup
    .string()
    .required("You need to confirm password")
    .oneOf(
      [yup.ref("password"), null],
      "Confirm password must be matched with password"
    ),
    
    })

  const {
    control,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm({
    defaultValues: {

      password: ""
    },
    resolver: yupResolver(schema),
  });
  const handleResetPasword = async(e) => {
    console.log("tesssst", e.password)
  //  e.preventDefault()
    try {
      // const password = e.target.elements.password.value
      // const confirm = e.target.elements.confirmPassword.value

     

      const result = await resetPass({
        password: e.password,
        id : id
      })

      if (result?.data?.result) {
        history.push('/login')
      }
      else
      {
        console.log(result?.data?.result?.msg);
      }
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <main>
      <section className="bg-soft d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
        <Container>
          <Row className="justify-content-center">
            <p className="text-center">
              <Card.Link as={Link} to={Routes.LoginPage.path} className="text-gray-700">
                <FontAwesomeIcon icon={faAngleLeft} className="me-2" /> Back to sign in
              </Card.Link>
            </p>
            <Col xs={12} className="d-flex align-items-center justify-content-center">
              <div className="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                <h3 className="mb-4">Đặt lại mật khẩu</h3>
                <Form onSubmit={handleSubmit(handleResetPasword)}>
                <Form.Group
                    id="password"
                    className={`mb-4 ${errors.password ? "error" : ""}`}
                  >
                    <Form.Label>Mật khẩu mới</Form.Label>
                    <InputGroup>
                      <InputGroup.Text>
                        <FontAwesomeIcon icon={faUnlockAlt} />
                      </InputGroup.Text>

                      <Controller
                        name="password"
                        control={control}
                        render={({ field }) => (
                          <Form.Control
                            name="password"
                            type="password"
                            placeholder="Mật khẩu mới"
                            {...field}
                          />
                        )}
                      />
                    </InputGroup>
                    <Form.Control.Feedback type="invalid">
                      {errors.password?.message}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group
                    id="confirmPassword"
                    className={`mb-4 ${errors.confirmPassword ? "error" : ""}`}
                  >
                    <Form.Label>Xác nhận</Form.Label>
                    <InputGroup>
                      <InputGroup.Text>
                        <FontAwesomeIcon icon={faUnlockAlt} />
                      </InputGroup.Text>
                      <Controller
                        name="confirmPassword"
                        control={control}
                        render={({ field }) => (
                          <Form.Control
                            name="confirmPassword"
                            type="password"
                            placeholder="Xác nhận"
                            {...field}
                          />
                        )}
                      />
                    </InputGroup>
                    <Form.Control.Feedback type="invalid">
                      {errors.confirmPassword?.message}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Button    disabled={isSubmitting}
                    variant="primary"
                    type="submit"
                    className="w-100">
                   Đặt lại mật khẩu
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </main>
  );
}
 
export default ResetPassword;

