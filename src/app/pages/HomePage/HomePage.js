import { Button, Alert } from "@themesberg/react-bootstrap";
import bannerBg from "app/assets/img/to-chuc-thi-scaled.png";
import banner from "app/assets/img/banner_bg1.png";
import "./HomePage.css";
const HomePage = () => {
  return (
    <>
      <div
        className="why-fe-vietnam d-flex justify-content-center"
        style={{ marginBottom: "60px" }}
      >
        <h1 className="text-center why-fe-title">Cơ hội và Mục tiêu</h1>
        <div className="d-flex justify-content-center">
          <p
            className="text-left"
            style={{
              width: "70%",
              fontSize: "26px",
              whiteSpace: "pre-wrap",
              marginBottom: "60px",
            }}
          >
            • Tăng cơ hội trúng tuyển: tham gia đánh giá năng lực không chỉ giúp bạn tăng khả năng trúng tuyển vào các trường Đại học mong muốn mà còn hỗ trợ thí sinh hiểu chính xác được khả năng hiện tại của bản thân.
            {`\n`}  {`\n`}• Tính toàn diện về mặt kiến thức: khác biệt so với kỳ thi THPT Quốc gia 2022, kỳ thi ĐGNL đòi hỏi thí sinh dự thi phải nắm vững kiến ​​thức được cung cấp trong chương trình THPT
            {`\n`}


          </p>


        </div>


        <h1 className="text-center why-fe-title">Cấu trúc bài đánh giá</h1>
        <div className="d-flex justify-content-center">
          <p
            className="text-left"
            style={{
              width: "70%",
              fontSize: "26px",
              whiteSpace: "pre-wrap",
              marginBottom: "20px",
            }}
          >
            <Alert key={`info`} variant={`success`}>

              <div
                style={{
                  whiteSpace: "pre-line",
                  color: "#0f5132",
                }}
                dangerouslySetInnerHTML={{
                  __html: '<p>Tổng cộng sẽ có 120 câu hỏi và thời gian làm bài là 2 giờ, phân bổ chủ yếu trong 3 phần:' + 
                  '<ul><li>Ngôn ngữ: 40 câu hỏi <ul><li>Tiếng Việt: 20 câu hỏi </li></ul> <ul><li>Tiếng Anh: 20 câu hỏi</li></ul> </li>' +
                  '<li>Toán học, tư duy logic, phân tích dữ liệu: 30 câu hỏi   <ul><li>Toán học: 10 câu hỏi </li></ul> <ul><li>Tư duy logic: 10 câu hỏi</li></ul> </li>  <ul><li>Phân tích số liệu: 10 câu hỏi </li></ul>  </li>' +
                  '<li>Giải quyết vấn đề: 50 câu hỏi   <ul><li>Hóa học: 10 câu hỏi </li></ul>   <ul><li>Vật lý: 10 câu hỏi </li></ul>   <ul><li>Sinh học: 10 câu hỏi </li></ul>   <ul><li>Địa lý: 10 câu hỏi </li></ul>   <ul><li>Lịch sử: 10 câu hỏi </li></ul>             </li></ul><p>Tất cả các câu hỏi đều thuộc loại câu hỏi một lựa chọn.</p>'

                }}



              ></div>

            </Alert>


          </p>


        </div>



      </div>



      {/* <div
        className="why-fe-vietnam module d-flex justify-content-center"
        style={{ background: `url(${banner})` }}
      >
        <h1 className="text-center why-fe-title">Why Exams ?</h1>
        <div className="module-list"></div>
      </div> */}
      <div
        className="why-fe-vietnam d-flex justify-content-center"
        style={{ padding: "60px" }}
      >
        <h1 className="text-center why-fe-title">
        Kỳ thi đánh giá năng lực Đại học Quốc gia TP.HCM
        </h1>
        <div className="module-list">
          <div
            className="module-container d-flex justify-content-between"
            style={{ width: "60%" }}
          >
            <div className="module-item">
              <img
                alt="module-item"
                src="https://blogdoanhnghiep.info/assets/data/res/tip-chia-se/group-la-gi-05.jpg"
              ></img>
              <h5> Thí sinh đã tham gia 400000+</h5>
            </div>
            <div className="module-item">
              <img
                alt="module-item"
                src="https://exam-practice.com/wp-content/uploads/2019/04/promotion-130x130.png"
              ></img>
              <h5> Thí sinh đã trúng tuyển 30000+</h5>
            </div>
            <div className="module-item">
              <img
                alt="module-item"
                src="https://celfa.vn/wp-content/uploads/2019/09/school.png"
              ></img>
              <h5>Đại học tham gia xét tuyển 81</h5>
            </div>
          </div>
        </div>
      </div>
      <div
        className="why-fe-vietnam module d-flex justify-content-center"
        style={{ background: `url(${banner})` }}
      >
        <h1 className="text-center why-fe-title">
        Bạn đã sẵn sàng để chinh phục ?
        </h1>
        <div className="d-flex justify-content-center my-4">
          <Button
            onClick={() => (window.location = "/login")}
            style={{ padding: "15px 30px", fontSize: "20px" }}
            variant="secondary"
          >
            Join us now!!
          </Button>
        </div>
      </div>
      {/* <WhatsNewPage />
      <ExamSchedule /> */}
    </>
  );
};

export default HomePage;
