import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCirclePlay } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";

const StudyRoadItem = ({ index, item }) => {
  const history = useHistory();
  return (
    <div className="LearningPathGroup_wrapper__PqkfS">
      <div
        className="d-flex justify-content-between title-container"
        onClick={() => history.push(`/topics/${item._id}`)}
      >
        <h2 className="LearningPathGroup_title__9Sige">{`${index + 1}. ${
          item.title
        }`}</h2>
        {/* <FontAwesomeIcon icon={faCirclePlay} /> */}
      </div>
      {item?.description !== undefined && (
        <div
          className="LearningPathGroup_desc__SnXOr"
          dangerouslySetInnerHTML={{ __html: item?.description }}
        ></div>
      )}
    </div>
  );
};

export default StudyRoadItem;
